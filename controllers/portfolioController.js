const Stock = require('../models/stock')

exports.getAll = (req, res, next) => {
  /* Here we just query the whole stock object for this User
   * Since we assume that there's only one user, Stock.find()
   * In real world, this would look like user.populate('stocks')...
   */
  Stock
    .find()
    .populate('trades', ['price', 'quantity', 'date', 'type'])
    .select('name trades')
    .then((results) => {
      res.json({ success: true, data: results })
    })
    .catch(next)
}

exports.getHoldings = (req, res, next) => {
  Stock
    .find()
    .populate('trades', ['price', 'quantity', 'date', 'type'])
    .select('name trades')
    .then((results) =>
      /* Map through each Stock and calculate holdings for its list of trades */
      results.map(e => {
        let stockHoldings = calculateHoldings(e.trades)
        return { name: e.name, quantity: stockHoldings.quantity, price: stockHoldings.price }
      })
    )
    .then((holdings) => res.json({ success: true, data: holdings }))
    .catch(next)
}

exports.getReturns = (req, res, next) => {
  Stock
    .find()
    .populate('trades', ['price', 'quantity', 'date', 'type'])
    .select('name trades')
    .then((results) =>
      /* Map through each Stock and calculate returns for its list of trades */
      results.map(e => {
        return { name: e.name, returns: computeReturns(e.trades) }
      })
    )
    .then((returns) => {
      res.json({ success: true, data: returns })
    })
    .catch(next)
}

/*
 * Given a list of trade object, returns the final holdings.
 * Returns an object of quantity
 */
function calculateHoldings(tradeList) {
  let value = 0
  let sumPrice = 0

  tradeList.forEach((trade) => {
    if (trade.type === 'BUY') value += trade.quantity
    else value -= trade.quantity
    sumPrice += trade.price
  })
  return { quantity: value, price: sumPrice / tradeList.length }
}

function computeReturns(tradeList) {
  let finalPrice = 100 // assumption
  let holdings = calculateHoldings(tradeList)
  let initialPrice = holdings.price
  /* Not sure if this formula is correct, sorry */
  return (finalPrice - initialPrice) * holdings.quantity
}
