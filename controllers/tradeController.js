const Stock = require('../models/stock')
const Trade = require('../models/trade')
/*
 * Assuming that req object includes a json object descibing the Trade to be added
 * {
 *   
 *   "trade": {
 *     "stock": xxx,
 *     "price": xx,
 *     "type": xxx,
 *     "quantity": xx,
 *     "date": xx, // not sure if this should come from the client, better to be filled here
 *    }
 * }
 */
exports.addTrade = (req, res, next) => {
  let tradeObj = req.body.trade
  if (tradeObj == null) return next('Missing { trade: }')
  let trade = new Trade(tradeObj)
  Stock
    .findOne({ name: tradeObj.stock }) // ensure that this is a valid stock
    .then((s) => {
      return trade.save()
    })
    .then(() => {
      res.json({ success: true, data: trade })
    })
    .catch(err => next(err))
}

/*
 * Assuming that req object includes a json object descibing the Trade to be added
 * {
 *   "tradeId": xx,
 *   "updatedFields": {
 *     "price": xx,
 *     "type": xxx,
 *     "quantity": xx,
 *     "date": xx, // not sure if this should come from the client, better to be filled here
 *    }
 * }
 */
exports.updateTrade = (req, res, next) => {
  let tradeId = req.body.tradeId
  let updatedFields = req.body.updatedFields
  if (tradeId == null) return next('Missing { tradeId: }')
  if (updatedFields == null) return next('Missing { updatedFields: }')

  Trade
    .updateOne({ _id: tradeId }, updatedFields)
    .then((t) => res.json({ success: true, data: updatedFields }))
    .catch(err => next(err))
}

exports.removeTrade = (req, res, next) => {
  let tradeId = req.body.tradeId
  if (tradeId == null) return next('Missing { tradeId: }')
  Trade
    .deleteOne({ _id: tradeId })
    .then((t) => res.json({ success: true, data: tradeId }))
    .catch(err => next(err))
}
