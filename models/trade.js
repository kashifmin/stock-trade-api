const mongoose = require('mongoose')

var TradeSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ['BUY', 'SELL'],
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  stock: {
    type: String,
    required: true
  },
})

module.exports = mongoose.model('Trade', TradeSchema)