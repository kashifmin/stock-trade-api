const mongoose = require('mongoose')
const Trade = require('./trade')

var StockSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  }
},
  // we want to be able to display 'trades' when we call res.json
  { toJSON: { virtuals: true } }
)
/*
 * stock.`trades` is only used for retrieval
 * hence it should be fine as a virtual
 * This will make things easier. No need to maintain an extra trade array
 * when a trades are updated.
*/
StockSchema.virtual('trades', {
  ref: 'Trade', // The model to use
  localField: 'name', // Find trades where `localField`
  foreignField: 'stock', // is equal to `foreignField`
  justOne: false,
  options: { sort: { date: -1 } }
})

module.exports = mongoose.model('Stock', StockSchema)
