# Main route /portfolio

## / - entire portfolio
```json
{
    "success": true,
    "data": [
        {
            "_id": "5c08be460217f92cd09bd09d",
            "name": "Reliance",
            "trades": [
                {
                    "_id": "5c08be460217f92cd09bd0a2",
                    "price": 135.6,
                    "date": "2018-12-06T06:14:28.183Z",
                    "quantity": 14,
                    "stock": "Reliance"
                },
                {
                    "_id": "5c08be460217f92cd09bd0a0",
                    "price": 124.4,
                    "date": "2018-12-06T06:14:28.183Z",
                    "quantity": 50,
                    "stock": "Reliance"
                },
                {
                    "_id": "5c08be460217f92cd09bd0a1",
                    "price": 133.4,
                    "date": "2018-12-06T06:14:28.183Z",
                    "quantity": 30,
                    "stock": "Reliance"
                }
            ],
            "id": "5c08be460217f92cd09bd09d"
        },
        {
            "_id": "5c08be460217f92cd09bd09f",
            "name": "KOTAK",
            "trades": [
                {
                    "_id": "5c08be470217f92cd09bd0a5",
                    "price": 23,
                    "date": "2018-12-06T06:14:28.183Z",
                    "quantity": 56,
                    "stock": "KOTAK"
                },
                {
                    "_id": "5c08be470217f92cd09bd0a4",
                    "price": 16.4,
                    "date": "2018-12-06T06:14:28.183Z",
                    "quantity": 20,
                    "stock": "KOTAK"
                },
                {
                    "_id": "5c08be470217f92cd09bd0a3",
                    "price": 20.4,
                    "date": "2018-12-06T06:14:28.183Z",
                    "quantity": 100,
                    "stock": "KOTAK"
                }
            ],
            "id": "5c08be460217f92cd09bd09f"
        }
    ]
}
```

## GET /holdings - holdings for each stock
```json
{
    "success": true,
    "data": [
        {
            "stock": "reliance",
            "quantity": 150,
            "price": 875.5
        },
        {
            "stock": "hdfc",
            "quantity": 100,
            "price": 1000
        },
    ]
}
```

## GET /returns - cumulative returns
```json
{
    "success": true,
    "data": [
        {
            "stock": "reliance",
            "returns": 60,
        },
        {
            "stock": "hdfc",
            "returns": 70,
        },
    ]
}
```

## POST /addTrade - add new trade
### Request body:
```json
{
    "stock": "reliance",
    "price": 88.33,
    "quantity": 33,
    "type": "SELL",
    "date": "js date string"
}
```

## POST /updateTrade - update trade
### Request body
```json
{
	"tradeId": "5c08c172b039706bbc09efa0",
	"updatedFields": {
		"stock": "Reliance",
		"price": 60,
		"quantity": 200,
		"type": "SELL",
		"date": "2018-12-05T11:04:07.998Z"
	}
}
```

## POST /removeTrade - remove trade
### Request body
```json
{
	"tradeId": "5c08c172b039706bbc09efa0"
}
```


# Schema design
Models: Stock, Trade, Portfolio
Points to note:
- ASSUMPTION: There is only one user and hence one portfolio
- A stock can have many trades
- A trade has only one stock
- Portfolio has a list of stocks and trades
- A trade can be created/updated/deleted
- Calculation of holdings and returns for a stock requires its list of trades

The schema is designed based on the points mentioned above, and may be changed if
there were more operations to be supported.
## Stock schema
- name
- virtual list of trades
 
## Trade schema
- stockname
- price
- quantity
- date
- type (ENUM: SELL, BUY)

My initial design included a list of object ids in Stock schema instead of a virtual. But this was problematic,
since I have to ensure consistency while making changes to a Trade object.
Since we don't have any update operations that has to be done directly on trade list, we can easily make it virtual. All values saved in the Trade object itself will be reflected in the list. The trade schema includes 
`stockname` using which the `trades` list of Stock actually populated by mongoose

# Error handling
- To keep things simpler, we will have only one global error handler that will respond
  with success: false and the error cause
- This is not safe in production ofcourse

# Tests
- There is some refractoring to be done to make this code testable
- Have a common db interface so that we can mock db instance and execute queries on it instead
- For each route, all the negative cases like null property tests have to be added
- Try to remove non existent trades
- Tests to see if added trades are reflected in portfolio
- Other basic functionaly and edge cases

# Project structure
project root
  | models -> contains mongoose Schema design for each model
  | routes -> defined supported routes
  | controllers -> route handler definitions
  index.js -> entry point
  db.js -> util function to connect to mongodb

# Improvements to be made
- db.js has to be refractored so that connections details could be set using some ENV
- Unwanted fields like _id must be removed from the json response
- Add actual error handling
- Null check wherever necessary
- Dockerize the whole setup


