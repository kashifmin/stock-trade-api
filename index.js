const portfolioRouter = require('./routers/portfolio')
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const connectDb = require('./db')

const PORT = process.env.PORT || 8331

function errorHandler(err, req, res, next) {
  console.log('Error: ', res)
  return res.json({ success: false, error: err })
}

app.use(bodyParser.json())
app.use('/portfolio', portfolioRouter)
app.use(errorHandler)

// TODO: make this return a Promise, handle errors better
connectDb()

app.listen(PORT, (res) => {
  console.log('Listening at : ' + PORT)
})
