const express = require('express')
const router = express.Router()
const portfolioController = require('../controllers/portfolioController')
const tradeController = require('../controllers/tradeController')

router.get('/', portfolioController.getAll)
router.get('/holdings', portfolioController.getHoldings)
router.get('/returns', portfolioController.getReturns)
router.post('/addTrade', tradeController.addTrade)
router.post('/updateTrade', tradeController.updateTrade)
router.post('/removeTrade', tradeController.removeTrade)

module.exports = router
