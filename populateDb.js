const connectDb = require('./db')
connectDb()

const Stock = require('./models/stock')
const Trade = require('./models/trade')

let stockObjects = [
  {
    name: 'Reliance',
    tradeObjs: [
      { price: 124.4, date: new Date(), type: 'BUY', quantity: 50, stock: 'Reliance' },
      { price: 133.4, date: new Date(), type: 'BUY', quantity: 30, stock: 'Reliance' },
      { price: 135.6, date: new Date(), type: 'SELL', quantity: 14, stock: 'Reliance' }
    ]
  },
  {
    name: 'HDFC',
    tradeObjs: [
      { price: 30.4, date: new Date(), type: 'BUY', quantity: 10, stock: 'HDFC' },
      { price: 32.4, date: new Date(), type: 'BUY', quantity: 20, stock: 'HDFC' },
      { price: 50.6, date: new Date(), type: 'SELL', quantity: 14, stock: 'HDFC' }
    ],
    trades: []
  },
  {
    name: 'KOTAK',
    tradeObjs: [
      { price: 20.4, date: new Date(), type: 'BUY', quantity: 100, stock: 'KOTAK' },
      { price: 16.4, date: new Date(), type: 'BUY', quantity: 20, stock: 'KOTAK' },
      { price: 23.0, date: new Date(), type: 'SELL', quantity: 56, stock: 'KOTAK' }
    ],
    trades: []
  }
]

Trade.deleteMany()
  .then(() => Stock.deleteMany())
  .then(() => stockObjects.forEach(e => {
    let stock = new Stock({ name: e.name })
    stock.save()
      .then(() => Trade.insertMany(e.tradeObjs))
  }))
  .then(() => console.log('Done!'))
