module.exports = () => {
  // Import the mongoose module
  var mongoose = require('mongoose')

  // Set up default mongoose connection
  var mongoDB = 'mongodb://test:test3214@ds157614.mlab.com:57614/stock_trade'
  mongoose.connect(mongoDB)
  // Get Mongoose to use the global promise library
  mongoose.Promise = global.Promise
  // Get the default connection
  var db = mongoose.connection

  // Bind connection to error event (to get notification of connection errors)
  db.on('error', console.error.bind(console, 'MongoDB connection error:'))
}
